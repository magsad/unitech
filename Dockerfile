FROM openjdk:11
MAINTAINER magsad
ARG JAR_FILE=build/libs/*T.jar
COPY ${JAR_FILE} /app/app.jar
WORKDIR /app/
ENTRYPOINT ["java","-jar","/app/app.jar"]