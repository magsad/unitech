package com.magsad.unitech.service;

import com.magsad.unitech.exception.AccountNotFoundException;
import com.magsad.unitech.model.entity.Account;
import com.magsad.unitech.model.entity.Transaction;
import com.magsad.unitech.model.enums.AccountStatus;
import com.magsad.unitech.model.enums.AccountType;
import com.magsad.unitech.model.enums.TransactionType;
import com.magsad.unitech.model.mapper.AccountMapper;
import com.magsad.unitech.payload.dto.AccountCreateDTO;
import com.magsad.unitech.payload.dto.AccountResponseDTO;
import com.magsad.unitech.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountService accountService;

    @Test
    void testObjectNotNull() {
        assertNotNull(accountService);
    }

    @Test
    void testFindAllSuccess() {
        when(accountRepository.findAll()).thenReturn(getList());

        List<AccountResponseDTO> accountResponseDTOList = accountService.getAccountsByUsername("22admin");
        assertEquals(accountResponseDTOList.size(), 0);
    }


    @Test
    void testFindByIdNotFoundException(){
        when(accountRepository.findByNumber("00000000000000000011")).thenThrow(new AccountNotFoundException("00000000000000000099"));
        assertThrows(AccountNotFoundException.class,
                ()->accountService.getAccountByNumber("00000000000000000011"));
    }

    List<Account> getList() {
        List<Account> accounts = new ArrayList<>();
        Account account1 = new Account();
        Account account2 = new Account();

        Transaction transaction = new Transaction();
        transaction.setTransactionId("5434r354-3535-4857485-344");
        transaction.setTransactionType(TransactionType.FUND_TRANSFER);

        accounts.add(account1);
        accounts.add(account1);
        return accounts;
    }

    Optional<AccountCreateDTO> getAccount() {
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO();
        accountCreateDTO.setAccountType(AccountType.valueOf("FUNDS_TRANSFER"));
        accountCreateDTO.setNumber("00000000000000000013");
        accountCreateDTO.setStatus(AccountStatus.valueOf("ACTIVE"));

        return Optional.of(accountCreateDTO);
    }

    @Test
    void testCreateAccountSuccess(){
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO();
        when(accountRepository.save(any())).thenReturn(accountCreateDTO);
        Account account = new Account();
        account.setStatus(AccountStatus.ACTIVE);
        assertTrue((BooleanSupplier) accountService.createAccount(accountCreateDTO));
    }

    @Test
    void testCreateAccountException(){
        AccountCreateDTO accountCreateDTO = new AccountCreateDTO();
        Account account = AccountMapper.INSTANCE.convertRequestToEntity(accountCreateDTO);
        when(accountRepository.save(account)).thenThrow(new RuntimeException());
        assertThrows(RuntimeException.class, () -> accountService.createAccount(accountCreateDTO));
    }
}
