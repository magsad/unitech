package com.magsad.unitech.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Long id) {
        super(String.format("User not found with id = %d",id));
    }
    public UserNotFoundException(String username) {
        super(String.format("User not found with username = %s",username));
    }

}
