package com.magsad.unitech.exception;

import java.math.BigDecimal;

public class CheckAccountsForTransferException extends RuntimeException{
    public CheckAccountsForTransferException(String message) {
        super(message);
    }

    public CheckAccountsForTransferException(String accountNumber, BigDecimal amount) {
        super(String.format("Account = %s - Funds is not enough to transfer! Minimal fund must be %s!",accountNumber,amount.toString()));
    }

}
