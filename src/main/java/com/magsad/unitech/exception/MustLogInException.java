package com.magsad.unitech.exception;

public class MustLogInException extends RuntimeException{
    public MustLogInException() {
        super("You must log in first!");
    }
}
