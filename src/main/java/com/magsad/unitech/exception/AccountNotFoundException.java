package com.magsad.unitech.exception;

public class AccountNotFoundException extends RuntimeException{
    public AccountNotFoundException(String number) {
        super(String.format("Account not found with number = %s",number));
    }
}
