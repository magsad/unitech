package com.magsad.unitech.exception;

public class DenyException extends RuntimeException{
    public DenyException() {
        super("Deny! You can only see your accounts!");
    }
}
