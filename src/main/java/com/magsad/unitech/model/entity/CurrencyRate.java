package com.magsad.unitech.model.entity;


import com.magsad.unitech.model.enums.AccountCurrency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "currency_rates")
public class CurrencyRate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private AccountCurrency currencyFrom;
    @Enumerated(EnumType.STRING)
    private AccountCurrency currencyTo;
    private Double rate;// currencyFrom/currencyTo







    //diger variant
    /*
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean success;
    @CreationTimestamp
    private LocalDateTime timestamp;
    @Enumerated(EnumType.STRING)
    private AccountCurrency base;
    @Enumerated(EnumType.STRING)
    private AccountCurrency currencyFrom;
    @Enumerated(EnumType.STRING)
    private AccountCurrency currencyTo;
    @ElementCollection
    private Map<AccountCurrency,Double> rates = new HashMap<>();*/
}
