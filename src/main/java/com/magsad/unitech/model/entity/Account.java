package com.magsad.unitech.model.entity;

import com.magsad.unitech.model.enums.AccountCurrency;
import com.magsad.unitech.model.enums.AccountStatus;
import com.magsad.unitech.model.enums.AccountType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "accounts",uniqueConstraints = {
        @UniqueConstraint(columnNames = "number")
}
)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min = 20,max = 20,message = "Must length of account number be 20 numbers!")//AZ46NABZ01350100000000015944
    private String number;

    @CreationTimestamp
    private LocalDateTime openedAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Enumerated(EnumType.STRING)
    private AccountCurrency accountCurrency;

    @Enumerated(EnumType.STRING)
    private AccountStatus status;

    private BigDecimal availableBalance;

    private BigDecimal actualBalance;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
