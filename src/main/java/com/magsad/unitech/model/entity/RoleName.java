package com.magsad.unitech.model.entity;

public enum RoleName {
  ROLE_USER,
  ROLE_ADMIN
}
