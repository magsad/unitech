package com.magsad.unitech.model.entity;

import com.magsad.unitech.model.enums.TransactionType;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    private String referenceNumber;//to

    private String transactionId;

//    @OneToOne(cascade = CascadeType.ALL)
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;//from

    @CreationTimestamp
    private LocalDateTime createdAt;

    public Transaction(BigDecimal amount, TransactionType transactionType, String referenceNumber, String transactionId, Account account) {
        this.amount = amount;
        this.transactionType = transactionType;
        this.referenceNumber = referenceNumber;
        this.transactionId = transactionId;
        this.account = account;
    }
}
