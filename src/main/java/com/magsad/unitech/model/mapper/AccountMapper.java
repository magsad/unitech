package com.magsad.unitech.model.mapper;

import com.magsad.unitech.model.entity.Account;
import com.magsad.unitech.payload.dto.AccountResponseDTO;
import com.magsad.unitech.payload.dto.AccountCreateDTO;
import com.magsad.unitech.payload.dto.AccountUpdateDTO;
import com.magsad.unitech.repository.UserRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", uses = { UserRepository.class })
public abstract class AccountMapper {

    public static final AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    @Mapping(source = "status", target = "status")
    @Mapping(source = "availableBalance", target = "availableBalance")
    @Mapping(source = "actualBalance", target = "actualBalance")
    public abstract Account convertToEntity(AccountUpdateDTO accountUpdateDTO);

    @Mapping(source = "number", target = "number")
    @Mapping(source = "accountType", target = "accountType")
    @Mapping(source = "accountCurrency", target = "accountCurrency")
    @Mapping(source = "status", target = "status")
    @Mapping(source = "availableBalance", target = "availableBalance")
    @Mapping(source = "actualBalance", target = "actualBalance")
    public abstract Account convertRequestToEntity(AccountCreateDTO accountCreateDTO);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "number", target = "number")
    @Mapping(source = "accountType", target = "accountType")
    @Mapping(source = "accountCurrency", target = "accountCurrency")
    @Mapping(source = "status", target = "status")
    @Mapping(source = "availableBalance", target = "availableBalance")
    @Mapping(source = "actualBalance", target = "actualBalance")
    public abstract AccountResponseDTO convertToDTO(Account account);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "number", target = "number")
    @Mapping(source = "accountType", target = "accountType")
    @Mapping(source = "accountCurrency", target = "accountCurrency")
    @Mapping(source = "status", target = "status")
    @Mapping(source = "availableBalance", target = "availableBalance")
    @Mapping(source = "actualBalance", target = "actualBalance")
    @Mapping(target = "user", ignore = true)
    public abstract List<AccountResponseDTO> convertToDTOList(List<Account> accounts);
}
