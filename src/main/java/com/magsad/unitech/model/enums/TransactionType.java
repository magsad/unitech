package com.magsad.unitech.model.enums;

public enum TransactionType {
    FUND_TRANSFER, UTILITY_PAYMENT
}
