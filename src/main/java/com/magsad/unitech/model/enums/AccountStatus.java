package com.magsad.unitech.model.enums;

public enum AccountStatus {
    ACTIVE, DEACTIVE
}
