package com.magsad.unitech.model.enums;

public enum AccountType {
    SAVINGS_ACCOUNT, FIXED_DEPOSIT, LOAN_ACCOUNT
}
