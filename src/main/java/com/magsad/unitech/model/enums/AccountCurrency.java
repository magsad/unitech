package com.magsad.unitech.model.enums;

public enum AccountCurrency {
    AZN,USD,EUR,GBP,RUB,TRY
}
