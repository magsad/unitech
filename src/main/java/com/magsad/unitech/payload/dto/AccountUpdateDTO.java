package com.magsad.unitech.payload.dto;

import com.magsad.unitech.model.enums.AccountCurrency;
import com.magsad.unitech.model.enums.AccountStatus;
import com.magsad.unitech.model.enums.AccountType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountUpdateDTO {
    private String number;
    private AccountStatus status;
    private BigDecimal availableBalance;
    private BigDecimal actualBalance;
}
