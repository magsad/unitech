package com.magsad.unitech.controller;

import com.magsad.unitech.exception.DenyException;
import com.magsad.unitech.exception.MustLogInException;
import com.magsad.unitech.payload.dto.AccountCreateDTO;
import com.magsad.unitech.payload.dto.AccountUpdateDTO;
import com.magsad.unitech.payload.response.MessageResponse;
import com.magsad.unitech.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("account")
@Api(tags = "Account CRUD operations")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class AccountController {
    AccountService accountService;

/*    @GetMapping("query")
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity getAccountsByUsernameQuery(@RequestParam String username,Principal principal) {
        try {
            if (!Objects.equals(principal.getName(), username)) {
                throw new DenyException();
            }
        }catch (NullPointerException e){
            throw new MustLogInException();
        }

        return ResponseEntity.ok(accountService.getAccountsByUsername(username));

    }*/


    @GetMapping("{username}")
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity getAccountsByUsername( @PathVariable String username) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info(auth.getName());
        try {
            if (!Objects.equals(auth.getName(), username)) {
                return ResponseEntity.ok(new MessageResponse("Deny! You can only see your accounts!"));
            }
        }catch (NullPointerException e){
            throw new MustLogInException();
        }
        return ResponseEntity.ok(accountService.getAccountsByUsername(username));
    }

    @GetMapping("{username}/{number}")
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity getAccountByNumber(@PathVariable String username,
                                             @PathVariable String number,
                                             Principal principal) {
        try {
            if (!Objects.equals(principal.getName(), username)) {
                return ResponseEntity.ok(new MessageResponse("Deny! You can only see your accounts!"));
            }
        }catch (NullPointerException e){
            throw new MustLogInException();
        }

        return ResponseEntity.ok(accountService.getAccountByNumber(number));

    }

    @PostMapping
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity createAccount(@RequestBody AccountCreateDTO accountCreateDTO){
        return ResponseEntity.status(HttpStatus.OK).body(accountService.createAccount(accountCreateDTO));
    }

    @PutMapping
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity updateAccount(@RequestBody AccountUpdateDTO accountUpdateDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(accountService.updateAccount(accountUpdateDTO));
    }

    @DeleteMapping("{number}")
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity deleteAccount(@PathVariable String number) {
        accountService.deleteAccount(number);
        return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(String.format("Account with number = %s has been deleted!",number)));
    }
}

