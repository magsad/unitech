package com.magsad.unitech.controller;


import com.magsad.unitech.exception.*;
import com.magsad.unitech.payload.response.MessageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
@Slf4j
public class MainControllerAdvice
{
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<MessageResponse> applicationException(UserNotFoundException e) {
        log.error(UserNotFoundException.class.toString());
        log.error(e.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(messageResponse);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<MessageResponse> applicationException(AccountNotFoundException e) {
        log.error(UserNotFoundException.class.toString());
        log.error(e.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(messageResponse);
    }

    @ExceptionHandler(DenyException.class)
    public ResponseEntity<MessageResponse> applicationException(DenyException e) {
        log.error(MustLogInException.class.toString());
        log.error(e.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(messageResponse);
    }

    @ExceptionHandler(MustLogInException.class)
    public ResponseEntity<MessageResponse> applicationException(MustLogInException e) {
        log.error(MustLogInException.class.toString());
        log.error(e.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(messageResponse);
    }

    @ExceptionHandler
    public ResponseEntity<MessageResponse> applicationException(ConstraintViolationException exception) {
        log.error(exception.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        exception.getConstraintViolations().forEach(v -> messageResponse.setMessage(v.getMessage()));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(messageResponse);
    }

    @ExceptionHandler(CheckAccountsForTransferException.class)
    public ResponseEntity<MessageResponse> applicationException(CheckAccountsForTransferException e) {
        log.error(MustLogInException.class.toString());
        log.error(e.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(messageResponse);
    }
}
