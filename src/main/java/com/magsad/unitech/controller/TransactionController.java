package com.magsad.unitech.controller;

import com.magsad.unitech.exception.DenyException;
import com.magsad.unitech.exception.MustLogInException;
import com.magsad.unitech.model.entity.Transaction;
import com.magsad.unitech.payload.dto.AccountCreateDTO;
import com.magsad.unitech.payload.dto.AccountUpdateDTO;
import com.magsad.unitech.payload.request.FundTransferRequest;
import com.magsad.unitech.payload.response.MessageResponse;
import com.magsad.unitech.service.AccountService;
import com.magsad.unitech.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("transaction")
@Api(tags = "Transaction CRUD operations")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class TransactionController {
    TransactionService transactionService;

    @PostMapping
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity fundTransfer(@RequestBody FundTransferRequest fundTransferRequest){
        return ResponseEntity.status(HttpStatus.OK).body(transactionService.fundTransfer(fundTransferRequest));
    }
}
