package com.magsad.unitech.controller;

import com.magsad.unitech.payload.request.RegisterRequest;
import com.magsad.unitech.repository.UserRepository;
import com.magsad.unitech.service.UserDetailsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("user")
@Api(tags = "User CRUD operations")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class UserController {
  UserRepository userRepository;
  UserDetailsServiceImpl userDetailsService;

  @GetMapping
  @PreAuthorize("hasRole('ADMIN') ")
  @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
  public ResponseEntity getUserList() {
    return ResponseEntity.ok(userDetailsService.loadAllUsers());
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') ")
  @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
  public ResponseEntity getUserById(@PathVariable Long id) {
      return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.loadUserById(id));
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') ")
  @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
  public ResponseEntity deleteUserById(@PathVariable Long id) {
    return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.deleteUserById(id));
  }

  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') ")
  @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
  public ResponseEntity updateUserById(@PathVariable Long id,
                                       @RequestBody RegisterRequest registerRequest) {
    return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.updateUserById(id,registerRequest));
  }
}
