package com.magsad.unitech.controller;

import com.magsad.unitech.exception.DenyException;
import com.magsad.unitech.exception.MustLogInException;
import com.magsad.unitech.service.CurrencyRateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Currency;
import java.util.Objects;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("currency-rates")
@Api(tags = "Currency rates CRUD operations")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class CurrencyRateController {
    CurrencyRateService currencyRateService;

    @GetMapping("/{currencyFrom}-{curencyTo}")
    public ResponseEntity getCurrencyRatePair(@PathVariable String currencyFrom,
                                                     @PathVariable String curencyTo
                                                     ) {
        String pair = String.format("%s/%s",currencyFrom,curencyTo);
        return ResponseEntity.ok(currencyRateService.getCurrencyConversion(currencyFrom,curencyTo));
    }

}
