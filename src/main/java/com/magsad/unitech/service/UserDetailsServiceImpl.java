package com.magsad.unitech.service;

import com.magsad.unitech.exception.UserNotFoundException;
import com.magsad.unitech.model.entity.RoleName;
import com.magsad.unitech.model.entity.Role;
import com.magsad.unitech.model.entity.User;
import com.magsad.unitech.payload.request.RegisterRequest;
import com.magsad.unitech.payload.response.MessageResponse;
import com.magsad.unitech.repository.RoleRepository;
import com.magsad.unitech.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserDetailsServiceImpl implements UserDetailsService {
  UserRepository userRepository;
  RoleRepository roleRepository;
  PasswordEncoder encoder;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

    return UserDetailsImpl.build(user);
  }

  @Transactional
  public List<UserDetails> loadAllUsers() {
    log.info("Started.. loadAllUsers()");
    List<User> users = userRepository.findAll();
    List<UserDetails> userDetailsList=  users.stream().map(user -> UserDetailsImpl.build(user)).collect(Collectors.toList());
    return userDetailsList;
  }

  @Transactional
  public UserDetails loadUserById(Long id) {
    log.info("Started.. getUserById() {}", id);
    User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
      return UserDetailsImpl.build(user);
  }

  public MessageResponse deleteUserById(Long id) {
    log.info("Started.. deleteUserById() {}", id);
    User userForDelete = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    String name = userForDelete.getFirstName();
    userRepository.delete(userForDelete);
    return new MessageResponse(String.format("user id = %s, name = %s has been deleted successfully",id,name));
  }

  public MessageResponse updateUserById(Long id, RegisterRequest registerRequest) {
    log.info("Started.. updateUserById() {}", id);
    User userForUpdate = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    String name = userForUpdate.getFirstName();

    //Update user
    User user = new User(registerRequest.getFirstName(),
            registerRequest.getLastName(),
            registerRequest.getFatherName(),
            registerRequest.getUsername(),
            registerRequest.getEmail(),
            encoder.encode(registerRequest.getPassword()));

    Set<String> strRoles = registerRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

            break;
          default:
            Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
      });
    }

    user.setRoles(roles);
//    userRepository.save(user);
    userForUpdate.setFirstName(user.getFirstName());
    userForUpdate.setLastName(user.getLastName());
    userForUpdate.setFatherName(user.getFatherName());
    userForUpdate.setUsername(user.getUsername());
    userForUpdate.setEmail(user.getEmail());
    userForUpdate.setPassword(user.getPassword());
    userForUpdate.setRoles(user.getRoles());

    userRepository.save(userForUpdate);
    UserDetailsImpl.build(userForUpdate);
    return new MessageResponse(String.format("user id = %s, name = %s has been updated successfully",id,name));
  }

}
