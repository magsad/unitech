package com.magsad.unitech.service;

import com.magsad.unitech.model.entity.CurrencyRate;
import com.magsad.unitech.model.enums.AccountCurrency;
import com.magsad.unitech.repository.CurrencyRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
@Slf4j
public class CurrencyRateService {
    CurrencyRepository currencyRepository;

    public String getCurrencyConversion(String currencyFrom, String currencyTo) {
        CurrencyRate currencyRate = new CurrencyRate();
        currencyRate.setCurrencyFrom(AccountCurrency.valueOf(currencyFrom));
        currencyRate.setCurrencyTo(AccountCurrency.valueOf(currencyTo));
        currencyRate.setRate(1.702);
        return String.format("%s/%s = %.4f",currencyFrom,currencyTo,currencyRate.getRate());
    }

    public List<CurrencyRate> getAll() {
       return currencyRepository.findAll();
    }
}
