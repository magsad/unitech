package com.magsad.unitech.service;

import com.magsad.unitech.exception.AccountNotFoundException;
import com.magsad.unitech.exception.CheckAccountsForTransferException;
import com.magsad.unitech.exception.DenyException;
import com.magsad.unitech.exception.MustLogInException;
import com.magsad.unitech.model.entity.Account;
import com.magsad.unitech.model.entity.CurrencyRate;
import com.magsad.unitech.model.entity.Transaction;
import com.magsad.unitech.model.enums.AccountStatus;
import com.magsad.unitech.model.enums.TransactionType;
import com.magsad.unitech.payload.request.FundTransferRequest;
import com.magsad.unitech.payload.response.MessageResponse;
import com.magsad.unitech.repository.AccountRepository;
import com.magsad.unitech.repository.CurrencyRepository;
import com.magsad.unitech.repository.TransactionRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class TransactionService {
    TransactionRepository transactionRepository;
    AccountRepository accountRepository;
    CurrencyRepository currencyRepository;

    public MessageResponse fundTransfer(FundTransferRequest fundTransferRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info(auth.getName());

        Account fromAccount = accountRepository.findByNumber(fundTransferRequest.getFromAccount()).orElseThrow(() -> new AccountNotFoundException(fundTransferRequest.getFromAccount()));
        log.info(fromAccount.getUser().getUsername());
        try {
            if (!fromAccount.getUser().getUsername().equals(auth.getName())) {
                throw new DenyException();
            }
        } catch (NullPointerException e) {
            throw new MustLogInException();
        }


        Account toAccount = accountRepository.findByNumber(fundTransferRequest.getToAccount()).orElseThrow(() -> new AccountNotFoundException(fundTransferRequest.getFromAccount()));

        checkBalance(fromAccount, toAccount, fundTransferRequest.getAmount());

        String transactionId = UUID.randomUUID().toString();

        if (!fromAccount.getAccountCurrency().equals(toAccount.getAccountCurrency())){
            CurrencyRate currencyRate = currencyRepository.findByCurrencyFromAndAndCurrencyTo(fromAccount.getAccountCurrency(),toAccount.getAccountCurrency()).get();
            log.warn(currencyRate.getId().toString());

            Double rate = currencyRate.getRate();

            BigDecimal rateFromDouble = new BigDecimal(rate);
            BigDecimal amount = fundTransferRequest.getAmount();
            BigDecimal result = rateFromDouble.multiply(amount);

            fromAccount.setActualBalance(fromAccount.getActualBalance().subtract(result));
            fromAccount.setAvailableBalance(fromAccount.getAvailableBalance().subtract(fundTransferRequest.getAmount()));
            accountRepository.save(fromAccount);

            transactionRepository.save(new Transaction(
                    fundTransferRequest.getAmount(),
                    TransactionType.FUND_TRANSFER,
                    toAccount.getNumber(),
                    transactionId,
                    fromAccount
            ));
        }else {
            fromAccount.setActualBalance(fromAccount.getActualBalance().subtract(fundTransferRequest.getAmount()));
            fromAccount.setAvailableBalance(fromAccount.getAvailableBalance().subtract(fundTransferRequest.getAmount()));
            accountRepository.save(fromAccount);

            transactionRepository.save(new Transaction(
                    fundTransferRequest.getAmount(),
                    TransactionType.FUND_TRANSFER,
                    toAccount.getNumber(),
                    transactionId,
                    fromAccount
            ));
        }

        toAccount.setActualBalance(toAccount.getActualBalance().add(fundTransferRequest.getAmount()));
        toAccount.setAvailableBalance(toAccount.getAvailableBalance().add(fundTransferRequest.getAmount()));
        accountRepository.save(toAccount);

        transactionRepository.save(new Transaction(
                fundTransferRequest.getAmount(),
                TransactionType.FUND_TRANSFER,
                fromAccount.getNumber(),
                transactionId,
                toAccount
        ));


        return new MessageResponse(String.format("Transaction successfully completed! transactionId = %s", transactionId));
    }

    private void checkBalance(Account fromAccount, Account toAccount, BigDecimal amount) {

        if (toAccount == null) {
            throw new CheckAccountsForTransferException("Account is not exist!");
        }

        if (fromAccount.getNumber().equals(toAccount.getNumber())) {
            throw new CheckAccountsForTransferException("Accounts can not be same!");
        }

        if (fromAccount.getStatus() == AccountStatus.DEACTIVE) {
            throw new CheckAccountsForTransferException(String.format("Account with number = %s is deactive!", fromAccount.getNumber()));
        }

        if (toAccount.getStatus() == AccountStatus.DEACTIVE) {
            throw new CheckAccountsForTransferException("Reference account status can not be deactive!");
        }

        if (fromAccount.getActualBalance().compareTo(BigDecimal.ZERO) < 0 || fromAccount.getActualBalance().compareTo(amount) < 0) {
            throw new CheckAccountsForTransferException(fromAccount.getNumber(), amount);
        }
    }
}
