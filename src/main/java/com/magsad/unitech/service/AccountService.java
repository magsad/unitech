package com.magsad.unitech.service;

import com.magsad.unitech.exception.AccountNotFoundException;
import com.magsad.unitech.exception.UserNotFoundException;
import com.magsad.unitech.model.entity.Account;
import com.magsad.unitech.model.entity.User;
import com.magsad.unitech.model.enums.AccountStatus;
import com.magsad.unitech.model.mapper.AccountMapper;
import com.magsad.unitech.payload.dto.AccountCreateDTO;
import com.magsad.unitech.payload.dto.AccountResponseDTO;
import com.magsad.unitech.payload.dto.AccountUpdateDTO;
import com.magsad.unitech.repository.AccountRepository;
import com.magsad.unitech.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
@Slf4j
public class AccountService {
    AccountRepository accountRepository;
    UserRepository userRepository;

    public List<AccountResponseDTO> getAccountsByUsername(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        List<Account> accounts =  accountRepository.findByUser(user).stream().filter(account -> account.getStatus()==AccountStatus.ACTIVE).collect(Collectors.toList());
        return AccountMapper.INSTANCE.convertToDTOList(accounts);
    }

    public AccountResponseDTO getAccountByNumber(String number) {
        Account account = accountRepository.findByNumber(number).get();
        return AccountMapper.INSTANCE.convertToDTO(account);
    }

    public AccountResponseDTO createAccount(AccountCreateDTO accountCreateDTO) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info(auth.getName());
        User user = userRepository.findByUsername(auth.getName()).orElseThrow(() -> new UserNotFoundException(auth.getName()));
        Account account = AccountMapper.INSTANCE.convertRequestToEntity(accountCreateDTO);
        account.setUser(user);
        return AccountMapper.INSTANCE.convertToDTO(accountRepository.save(account));

    }

    public AccountResponseDTO updateAccount(AccountUpdateDTO accountUpdateDTO) {
        Account account =  accountRepository.findByNumber(accountUpdateDTO.getNumber()).orElseThrow(() -> new AccountNotFoundException(accountUpdateDTO.getNumber()));
        log.info(account.getId().toString());
        account.setStatus(accountUpdateDTO.getStatus());
        account.setAvailableBalance(accountUpdateDTO.getAvailableBalance());
        account.setActualBalance(accountUpdateDTO.getActualBalance());
        return AccountMapper.INSTANCE.convertToDTO(accountRepository.save(account));
    }

    public void deleteAccount(String number) {
        Account account =  accountRepository.findByNumber(number).orElseThrow(() -> new AccountNotFoundException(number));
        accountRepository.delete(account);
    }
}
