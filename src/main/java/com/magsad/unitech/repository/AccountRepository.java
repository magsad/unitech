package com.magsad.unitech.repository;

import com.fasterxml.jackson.annotation.OptBoolean;
import com.magsad.unitech.model.entity.Account;
import com.magsad.unitech.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
    List<Account> findByUser(User user);
    Optional<Account> findByNumber(String number);
}
