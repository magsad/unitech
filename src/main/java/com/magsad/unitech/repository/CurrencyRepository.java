package com.magsad.unitech.repository;

import com.magsad.unitech.model.entity.CurrencyRate;
import com.magsad.unitech.model.enums.AccountCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyRate,Long> {
    Optional<CurrencyRate> findByCurrencyFromAndAndCurrencyTo(AccountCurrency currencyFrom, AccountCurrency currencyTo);
}
