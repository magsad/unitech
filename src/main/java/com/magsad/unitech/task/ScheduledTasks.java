package com.magsad.unitech.task;

import com.magsad.unitech.service.CurrencyRateService;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;

@Component
@Slf4j
public class ScheduledTasks {
    CurrencyRateService currencyRateService;

    @Scheduled(cron = "0 * * * * *")//every 1 minute
    @SchedulerLock(name = "Currency API",
            lockAtLeastForString = "PT5S", lockAtMostForString = "PT50S")//lock min 30 sec, max 50 sec
    public void refreshCurrencyAPI() {
        log.info("Scheduled task started ..");
    }

    /*
    Mock service for currency rates
   1. Currency API -> openfeign read
   2. Kafka Produce -> Consume
    */
}
